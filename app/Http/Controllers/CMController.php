<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContratoMenor;
use App\Models\DocCM;
use Illuminate\Support\Facades\DB;

class CMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $CM = ContratoMenor::all();
        $DCM = DocCM::join('ContratoMenor', 'ContratoMenor.idContratoMenor', '=', 'DocumentosCM.ContratoMenor_idContratoMenor')
            ->select('DocumentosCM.*')->get();
        return view('CM/inicio', ['CM' => DB::table('ContratoMenor')->paginate(4)], compact('CM', 'DCM'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        return view('CM/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
        $CM = new ContratoMenor();
        $CM->nombre = $request->input('nombre');
        $CM->save();

        $DCM = new DocCM();
        $DCM->Titulo = $request->input('Titulo');
        if (null !== $request->file('Ruta')) {
            $DCM->Ruta = asset('storage/' . $request->file('Ruta')->hashName());
            $request->file('Ruta')->store('public');
        };
        $DCM->ContratoMenor_idContratoMenor = $CM->idContratoMenor;
        $DCM->save();
        return redirect('CM/inicio');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate1()
    {
        return view('CM/createV');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate1(Request $request)
    {
        $CM = ContratoMenor::create($request->all());
        $CM->nombre = $request->input('nombre');
        $DCM = DocCM::create($request->all());
        if (null !== $request->file('doc')) {
            $DCM->imagen = asset('storage/' . $request->file('doc')->hashName());
            $request->file('doc')->store('public');
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        $DM = DocCM::findOrFail($id);
        $CMs = ContratoMenor::all();
        return view('CM/edit', compact('CMs', 'DM'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function putEdit(Request $request, $id)
    {
        $DM = DocCM::findOrFail($id);
        $DM->Titulo = $request->input('Titulo');
        if (null !== $request->file('Ruta')) {
            $DM->Ruta = asset('storage/' . $request->file('Ruta')->hashName());
            $request->file('Ruta')->store('public');
        };
        $DM->save();
        return redirect('CM/inicio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyD($id)
    {
        $DCM = DocCM::where('ContratoMenor_idContratoMenor',$id);
        $DCM->delete();
        return redirect('CM/inicio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyC($id)
    {
        $CM = ContratoMenor::where('idContratoMenor',$id);
        $CM->delete();
        return redirect('CM/inicio');
    }
}
