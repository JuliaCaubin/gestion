<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idDocumentos
 * @property string $Titulo
 * @property string $Ruta
 * @property string $Fecha creacion
 * @property string $Fecha ultima version
 * @property int $ContratoMenor_idContratoMenor
 * @property ContratoMenor $contratoMenor
 */
class DocCM extends Model
{
    public $timestamps=false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'DocumentosCM';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idDocumentos';

    /**
     * @var array
     */
    protected $fillable = ['Titulo', 'Ruta', 'Fecha creacion', 'Fecha ultima version', 'ContratoMenor_idContratoMenor'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contratoMenor()
    {
        return $this->belongsTo('App\Models\ContratoMenor', 'ContratoMenor_idContratoMenor', 'idContratoMenor');
    }
}
