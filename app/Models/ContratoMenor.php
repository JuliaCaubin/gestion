<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\DocCM;

/**
 * @property int $idContratoMenor
 * @property string $Nombre
 * @property string $Fecha
 */
class ContratoMenor extends Model
{
    public $timestamps=false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ContratoMenor';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idContratoMenor';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'fecha'];

    public function documentosCM()
    {
        return $this->hasMany('App\Models\DocCM', 'ContratoMenor_idContratoMenor', 'idContratoMenor');
    }
}
