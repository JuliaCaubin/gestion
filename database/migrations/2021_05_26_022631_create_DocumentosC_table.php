<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentosCTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentosC', function (Blueprint $table) {
            $table->integer('idDocumentos', true);
            $table->string('Titulo', 45)->nullable();
            $table->string('Ruta')->nullable();
            $table->date('Fecha')->nullable()->default(now());
            $table->integer('Contrato_idContrato')->index('fk_Documentos_Contrato1_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DocumentosC');
    }
}
