<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDocumentosCMTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DocumentosCM', function (Blueprint $table) {
            $table->foreign('ContratoMenor_idContratoMenor', 'fk_DocumentosCM_ContratoMenor1')->references('idContratoMenor')->on('ContratoMenor')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DocumentosCM', function (Blueprint $table) {
            $table->dropForeign('fk_DocumentosCM_ContratoMenor1');
        });
    }
}
