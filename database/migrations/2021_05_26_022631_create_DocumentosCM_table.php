<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentosCMTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentosCM', function (Blueprint $table) {
            $table->integer('idDocumentos', true);
            $table->string('Titulo', 45)->nullable();
            $table->string('Ruta')->nullable();
            $table->date('Fecha')->nullable()->default(now());
            $table->integer('ContratoMenor_idContratoMenor')->index('fk_DocumentosCM_ContratoMenor1_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DocumentosCM');
    }
}
