<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDocumentosCNVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DocumentosCNV', function (Blueprint $table) {
            $table->foreign('Convenio_idConvenio', 'fk_Documentos_Convenio1')->references('idConvenio')->on('Convenio')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DocumentosCNV', function (Blueprint $table) {
            $table->dropForeign('fk_Documentos_Convenio1');
        });
    }
}
