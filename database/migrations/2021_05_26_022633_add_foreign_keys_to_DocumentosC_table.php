<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDocumentosCTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DocumentosC', function (Blueprint $table) {
            $table->foreign('Contrato_idContrato', 'fk_Documentos_Contrato1')->references('idContrato')->on('Contrato')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DocumentosC', function (Blueprint $table) {
            $table->dropForeign('fk_Documentos_Contrato1');
        });
    }
}
