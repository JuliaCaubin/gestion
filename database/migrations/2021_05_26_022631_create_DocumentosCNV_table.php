<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentosCNVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentosCNV', function (Blueprint $table) {
            $table->integer('idDocumentos', true);
            $table->string('Titulo', 45)->nullable();
            $table->string('Ruta')->nullable();
            $table->date('Fecha')->nullable()->default(now());
            $table->integer('Convenio_idConvenio')->index('fk_Documentos_Convenio1_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DocumentosCNV');
    }
}
