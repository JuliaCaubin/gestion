<?php

namespace Database\Seeders;

use App\Models\ContratoMenor;
use App\Models\DocCM;
use Illuminate\Database\Seeder;

class DCMSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocCM::create(['Titulo' => 'd1', 'Ruta' => 'ruta del docuemnto', 'Fecha' => '2020-01-05', 'ContratoMenor_idContratoMenor' => 1]);
        DocCM::create(['Titulo' => 'd2', 'Ruta' => 'ruta del docuemnto1', 'Fecha' => '2020-01-05', 'ContratoMenor_idContratoMenor' => 2]);
    }
}
