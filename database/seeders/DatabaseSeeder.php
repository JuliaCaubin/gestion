<?php

namespace Database\Seeders;

use App\Models\ContratoMenor;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CMSeeder::class);
        $this->call(DCMSeeder::class);
    }
}
