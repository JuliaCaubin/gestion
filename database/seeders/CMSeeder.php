<?php

namespace Database\Seeders;

use App\Models\ContratoMenor;
use Illuminate\Database\Seeder;

class CMSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContratoMenor::create(['Nombre' => 'Jesus', 'Fecha' => '2000-01-05']);
        ContratoMenor::create(['Nombre' => 'Pedro', 'Fecha' => '2001-01-05']);
    }
}
