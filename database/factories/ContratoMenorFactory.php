<?php

namespace Database\Factories;

use App\Models\ContratoMenor;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContratoMenorMFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ContratoMenor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'Nombre' => $this->faker->name,

            'Fecha' => now(),
        ];
    }
}
