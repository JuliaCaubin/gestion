@extends('adminlte::page')

@section('title', 'Cabildo')

@section('content_header')
<div class="container-fluid">
    <a href="{{url('/CM/inicio')}}">
        < Volver</a>
            <h1 style="display: inline-block; padding-left: 30%">Crear contrato menor</h1>
</div>
@stop

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form action="" method="post" enctype="multipart/form-data" style="text-align: center;">
                    @CSRF
                    <div class="row">
                        <div class="col-25">
                            <label for="nombre">Nombre: </label>
                        </div>
                        <div class="col-75">
                            <input type="text" id="nombre" name="nombre" class="form-control" value="{{old('nombre')}}">
                        </div>
                    </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <table id="t2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <form action=""><input type="checkbox"></form>
                                    </td>
                                    <td>Documento 1</td>
                                    <td>05/05/2021</td>
                                </tr>
                                <tr>
                                    <td>
                                        <form action=""><input type="checkbox"></form>
                                    </td>
                                    <td style="text-align: center;">
                                        <div class="form-group">
                                            <label for="doc">Selecciona un documento</label>
                                            <input type="file" name="doc" id="doc">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-primary" style="padding:7px 80px;">
                                                Guardar
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </form>
        </div>
        <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<style>
    input[type=text] {
        width: 98%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }

    label {
        padding: 8px 10px 10px 0;
        display: inline-block;
    }

    .col-25 {
        float: left;
        width: 25%;
        margin-top: 6px;
    }

    .col-75 {
        float: left;
        width: 75%;
        margin-top: 6px;
    }
      .sidebar-dark-light{
        background: #17a2b8 !important;
      }
</style>
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('.t1').DataTable({
            "lengthChange": false,
            "pageLength": 5,
            "paging": false,
            "searching": false,
            "info": false,
        })
    });
</script>
@stop