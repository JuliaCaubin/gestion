@extends('adminlte::page')

@section('title', 'Cabildo')

@section('content_header')
<div class="container-fluid">
    <h1 style="display: inline-block">Contratos menores</h1>
    <button style="margin-left:13px"><a href="{{url('/CM/create')}}">Crear</a></button>
</div>
@stop

@section('content')
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            @foreach($CM as $CMs)
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><a href="#">{{$CMs->nombre}}</a></h3>
                        <form method="POST" action="{{url('/CM/deleteC').'/'.$CMs->idContratoMenor}}" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" role="button" style="float: right;">
                                Borrar
                            </button>
                        </form>
                        <!-- <button style="float: right;"><a href="{{url('/CM/createV')}}">Crear</a></button>-->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-hover; t1">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($DCM as $DM)
                                @if($CMs->idContratoMenor == $DM->ContratoMenor_idContratoMenor)
                                <tr>
                                    <td>{{$DM->Titulo}}</td>
                                    <td>{{$DM->Fecha}}</td>
                                    <td style="text-align: center;"><button><a href="{{url('/CM/edit').'/'.$DM ->idDocumentos}}">Editar</a></button></td>
                                    <td style="text-align: center;">
                                        <form method="POST" action="{{url('/CM/deleteD').'/'.$DM ->idDocumentos}}" style="display:inline">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" role="button" style="float: right;">
                                                Borrar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @else
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            @endforeach
            {{$CM = DB::table('ContratoMenor')->simplePaginate(4)}}
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<style>
    .sidebar-dark-light {
        background: #17a2b8 !important;
    }
</style>
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('.t1').DataTable({
            "lengthChange": false,
            "pageLength": 5,
            "paging": false,
            "searching": false,
            "info": false,
        })
    });
</script>
@stop