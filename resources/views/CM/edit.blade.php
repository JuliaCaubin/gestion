@extends('adminlte::page')

@section('title', 'Cabildo')

@section('content_header')
<div class="container-fluid" style="display: inline-block;">
    <a style="display: inline-block;" href="{{url('/CM/inicio')}}">
        < Volver</a>
            <h1 style="display: inline-block; padding-left: 30%">Editar {{$DM->Titulo}}</h1>
</div>
@stop

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form action="" method="post" enctype="multipart/form-data" style="text-align: center;">
                    @method('PUT')
                    @CSRF
                    <div class="row">
                        <div class="col-25">
                            <label for="Titulo">Titulo: </label>
                        </div>
                        <div class="col-75">
                            <input type="text" id="Titulo" name="Titulo" class="form-control" value="{{old('$DM->Titulo')}}">
                        </div>
                    </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-outline card-info">
                    <div class="card-body">
                        <textarea name="tiny" class="tiny"></textarea>
                    </div>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary" style="padding:7px 80px;">
                        Guardar
                    </button>
                </div>
            </div>
            <!-- /.card -->
            </form>
        </div>
        <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content -->
@stop
@section('plugins.Summernote', true)

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<style>
    input[type=text] {
        width: 98%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        resize: vertical;
    }

    label {
        padding: 8px 10px 10px 0;
        display: inline-block;
    }

    .col-25 {
        float: left;
        width: 25%;
        margin-top: 6px;
    }

    .col-75 {
        float: left;
        width: 75%;
        margin-top: 6px;
    }

    .sidebar-dark-light {
        background: #17a2b8 !important;
    }
</style>
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
<script src="https://cdn.tiny.cloud/1/besnhii11zyybwd8bpajbvzoiac2t5780lp9mzafw4zw63x9/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    $(document).ready(function() {
        $('.t1').DataTable({
            "lengthChange": false,
            "pageLength": 5,
            "paging": false,
            "searching": false,
            "info": false,
        })
    });
    tinymce.init({
        selector: '.tiny',
        plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinymcespellchecker',
        toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table'
    });
</script>
@stop