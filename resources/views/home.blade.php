@extends('adminlte::page')

@section('title', 'Cabildo')

@section('content_header')
<h1>Home</h1>
@stop

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"> <a href="{{url('/CM/inicio')}}">Contrato Menor</a></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-hover; t1">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($CM as $CMs)
                                <tr>
                                    <td><a href="{{url('/CM/show')}}">{{$CMs->nombre}}</a></td>
                                    <td>{{$CMs->fecha}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Contrato</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-hover; t1">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Contrato 1</td>
                                    <td>05/05/2021</td>
                                </tr>
                                <tr>
                                    <td>Contrato 2</td>
                                    <td>08/05/2021</td>
                                </tr>
                                <tr>
                                    <td>Contrato 3</td>
                                    <td>10/05/2021</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Convenio</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-hover; t1">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Convenio</td>
                                    <td>05/05/2021</td>
                                </tr>
                                <tr>
                                    <td>Convenio</td>
                                    <td>08/05/2021</td>
                                </tr>
                                <tr>
                                    <td>Convenio</td>
                                    <td>10/05/2021</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<style>
      .sidebar-dark-light{
        background: #17a2b8 !important;
      }
 </style>
@stop

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('.t1').DataTable({
            "lengthChange": false,
            "pageLength": 5,
            "searching": false,
            "info": false,
        })
    });
</script>
@stop