<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'getShow'])->name('home');
Route::get('/CM/inicio', [App\Http\Controllers\CMController::class, 'getIndex'])->name('index');
Route::get('/CM/create', [App\Http\Controllers\CMController::class, 'getCreate'])->name('create');
Route::post('/CM/create', [App\Http\Controllers\CMController::class, 'postCreate'])->name('create');
Route::get('/CM/createV', [App\Http\Controllers\CMController::class, 'getCreate1'])->name('create');
Route::post('/CM/createV', [App\Http\Controllers\CMController::class, 'postCreate1'])->name('create');
Route::get('/CM/edit/{id?}', [App\Http\Controllers\CMController::class, 'getEdit'])->name('edit');
Route::delete('/CM/deleteD/{id?}', [App\Http\Controllers\CMController::class, 'destroyD']);
Route::delete('/CM/deleteC/{id?}', [App\Http\Controllers\CMController::class, 'destroyC']);